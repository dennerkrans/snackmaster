const drinks = require('./drinks');
const names = require('./names');
const poisons = require('./poisons');
const trinkets = require('./trinkets');

module.exports = {
  drinks,
  names,
  poisons,
  trinkets
};
