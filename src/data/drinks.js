module.exports = [
  {
    name: 'Goblin Grog',
    color: 'Putrid Green',
    description:
      'The cheapest rot gut ale in the land.  This is the stuff that can be brewed with all types of ingredients sometimes including goblin.  Taste is terrible but gets the job done',
    effect:
      'Any adventurer who drinks this will not regain HP on a Long Rest.  They will be too busy vomiting and feeling awful all night.  Effect wears off after 1 day.',
    price: '1 Silver'
  },
  {
    name: 'Griffin Nest',
    color: 'Medium Brown',
    description: 'A shot of Dwarven Brandy with a raw egg in it.',
    effect: 'DC 10 CON save or vomiting ensues.',
    price: '5  Copper'
  },
  {
    name: 'Skullbuster XXX',
    color: 'Pink with brown lumps',
    description:
      'A potent orcish ale where the vintage is measured by how many lumps in it, the more the better.  Normally only given as a rare gift by people who have done the orcish people a great service',
    effect: 'For non-orcs they must make a DC15 con save or pass-out for 1D6 hours, half-orcs make the save at DC7',
    price: 'Free (see description)'
  },
  {
    name: "Clockwhistle's Surprise",
    color: 'Dark Purple, Glittery',
    description:
      'A nice tasting potion that became a widely served drink purely for the fun of it. Invented by Dondaan Clockwhistle, this drink is said to cause strange effects, most of the time anyways.',
    effect:
      'DC 15 CON save. Roll on Wild Magic table upon fail, rerolling for effects that directly harm someone. DM can also decide what happens. Effect can not be affected by features granted to Wild Magic Sorcerers.',
    price: '5 Silver per Mug'
  },
  {
    name: "Silver's Ale",
    color: 'Clear',
    description:
      "The pride and joy of the Silvermane Clan, Silver's Ale is a powerful Dwarven concoction of 296% ABV. The drink can also double as a splash weapon in a bar fight!",
    effect:
      'DC 15 CON save for 2d4 poison damage for non-Dwarves. If used as improvised weapon, 2d4 Acid damage to anyone except Dragonborn, DEX save for half.',
    price: '3 Silver per Pint'
  },
  {
    name: 'Glowglass',
    color: 'green phosphorescent',
    description:
      'A gnomish concoction made from glowing mushrooms. Most non gnomes refuse to drink this sweet liquor, most gnomes laugh at this.',
    effect: 'Drinker sheds a soft glow like a candle for 1d4 hours.',
    price: '2 Silver a Shot'
  },
  {
    name: 'Old Stinky',
    color: 'Brown with floating debris',
    description:
      'A strong fungus ale, brewed by Eblo Harglet after he developed a taste for fungus ales in the Underdark.',
    effect:
      "In the next 24 hours, a patch of mushrooms will grow from a random location on the player's body.  Harmless. This patch will continue to grow mushrooms for 1d4 days or can be cured by magical healing.",
    price: '1 Copper'
  },
  {
    name: 'Roaring Eagle Ale',
    color: 'Blood red with various fine chopped cloves',
    description:
      'A strong hearty Ale with a strong burning finish. No one knows why it is blood red in color. Possibly because of a reaction of the water they use in the brewing process, or because they barrels they use are made with Blood Root trees. Which grow special in the region where The Roaring Eagle is. Specialty at "The Roaring Eagle" Shipped around the world.',
    effect:
      'DC 12 Con Save 1d6 fire damage upon fail. If succeeded you gain fire resistance for 16 hours. (At least a mug must be drank to trigger either effect.)',
    price: '2 Silver'
  },
  {
    name: 'Slog',
    color: 'Dark brown',
    description:
      'This drink is taken from the waste barrel. All the leftover, half drunk, or not made correctly alcohol is thrown into the barrel to be reused very cheap. the taste changes daily based off the most unused alcohol that night.',
    effect: 'DC 8 con save to keep the random contents down.',
    price: '3 Copper'
  },
  {
    name: 'Bilgewater',
    color: 'Blue-ish Brown or Jet Black',
    description:
      'Made by soaking the inner organs of various sea creatures soaking in a fermented brine, the concoction is the run through a sieve to remove the bones and viscera',
    effect: 'The lingering taste of rotten, brackish seafood remains on the palette for a few hours',
    price: '5 Copper or One Tall Tale'
  },
  {
    name: 'Deepminer',
    color: 'shiny golden liquid with black foam',
    description:
      'Taken from a keg with floating lumps of coal, initial strong charcoal taste followed by delicious warm honey golden taste',
    effect:
      'Upon your next Long Rest, your character makes a bowel movement.  You must roll d100 if you roll under the number of drinks consumed your feces contains a small gold nugget worth d10 gold, if not  is a painful coal filled movement',
    price: '10 Silver per Pint'
  },
  {
    name: 'Sildars itch',
    color: 'clear with white shimmer',
    description:
      'Clear shot, strong apple and clove taste (drink came from a distiller that had itchy back but nobody to scratch it)',
    effect: 'upon drinking hands get very warm and fingernails grow d4 cm',
    price: '2 Silver'
  },
  {
    name: 'Dragonfire Brandy',
    color: 'Deep candy apple red w, swirling orange mist',
    description: 'Aged brandy with a hint of Apple Cider / Red hots.  Smooth',
    effect:
      "DC 13 Con sv.  Fail = Disadvantage on persuasion and seduction resistance tests, but you're happy about whatever you did... until tomorrow. Maybe. I mean you DID have fun. And the Tiefling twins were seriously alluring and sexy.",
    price: '1 Gold per Snifter.'
  },
  {
    name: 'The Cranky Clergyman',
    color: 'Deep Opaque Black with Stark White Head',
    description:
      "Adapted from a well aged potion of cure wounds, this bitter drink feels and tastes like poison before entering stomach. A brief moment later, warm feelings emanate from the drinker's gut as some small wounds seal up. A tavern favorite for after-brawl drinks.",
    effect:
      'DC 11 Con Save, 1d4 poison damage upon fail. One round later (approx. five seconds) later, heals 2d4 hit points',
    price: '7 Copper'
  },
  {
    name: "Milligun's Deep Stout",
    color: 'Deep chocolate to black with a white frothy head',
    description:
      "A brew hailing from one of the most revered Dwarven breweries, Milligun's is their signature brew. Even uncommon in Dwarven communities, it is rare to find in human establishments. A complex, deep, hearty flavor lingering of coffee and burnt grain with a bold body and a hint of plum.",
    effect: 'Advantage on saving throws vs. Fear for the next 1hr',
    price: '1 Gold'
  },
  {
    name: "Rado's Death Punch",
    color: 'Blood red with a blue top layer',
    description:
      "The best blend of magic and rum in the world. Dark rum mixed with a cherry cordial in a silver strainer imbued with magic, it tastes like a perfect wine and rum mix. To add the blue topper, simply stir with a golden spoon and the magic reacts to the gold, giving a random flavor each time it's poured.",
    effect:
      'DC 14 Con Save, 1d10 poison on a fail and become drunk for 1d4 hours. On a success, half damage and must succeed on a DC 16 Wisdom save to ask for another which caused the consumer to have disadvantage on the next Con save.',
    price: '2 Silver per Shot. If already consumed one shot, price doubles.'
  },
  {
    name: 'Jelly Drink',
    color: 'A greenish drink that wobbles when moved',
    description:
      "It's as it sounds like. The drink is comprised of a green minty liquid filled with gelatinous cubes that can either be chewed or swallowed whole. The drink itself is a little sour but with the sweetness of the cubes in contrast, it makes it a rather unique and flavoursome experience.",
    effect: 'Allows sour food to taste sweet and visa versa. Effect lasts for only 30 mins per drink.',
    price: '10 Copper'
  },
  {
    name: "Ork's blood brew",
    color: 'Blood red with crimson froth',
    description:
      "This salty bitter brew is made from the fermented blood of an orc chief's rivals. after the first few gulps the drink feels thick and coppery. Brewed by orcish shaman.",
    effect:
      'con save 11 + 1 for every additional mug to become nauseous. if you consume another tankard after becoming nauseous, you vomit.',
    price: '8 Copper'
  },
  {
    name: "Poorman's Wine",
    color: 'Watered down milk lacking in consistency',
    description:
      "A drink made from potatoes, milk and some other kind of ingredients that you don't really want to think about. Raw and lacking in finesse and taste, it does the job it's supposed to - getting you blazing drunk... and maybe forgetting about your old ball and chain of a wife.",
    effect: 'Allows the consumer to forget one painful recent memory(in the last two weeks) for 1d6 in hours.',
    price: '1 Copper'
  },
  {
    name: 'The Hairy Wizard',
    color: 'Various shades of brown',
    description: 'Strong to the taste but can leave an itchy sensation in the throat',
    effect:
      'Roll 1d6 for every cup you drink. On a 5 or 6 your throat starts to become itchy. Sensation wears off after a time. After <0.5*CON Score> cups, make a DC 15 Con save. On a save you become drunk for 1d4 hours. On a fail, you vomit hairballs like a cat.',
    price: '3 Silver'
  },
  {
    name: "Warlock's Wasted Patron",
    color: 'Blue with dark swirls',
    description: 'Burns all the way down, but has an amazing aftertaste.',
    effect: 'Those who drink this drink can use Mage Hand (cantrip) at will for the next 24 hours',
    price: '1 Gold'
  },
  {
    name: "Kossuths' Kiss",
    color: 'Red and Flamey',
    description: 'Cinnamon Schnapps with a special proprietary ingredient the bartender lights on fire before serving',
    effect: 'None. Feels nice and warm though.',
    price: '5 Silver'
  },
  {
    name: 'Firewine',
    color: 'Pinot Noir',
    description:
      'This ruby rich delight is packed with mouth-watering sumptuousness with hints of bramble, blackberry, boysenberry, Don Cherry and Frankenberry flourishes. A treat to eat with beef testicles or lamb spleen escabeche. Also an ideal companion for manic-depression. Shows promise to last longer than your belief in an afterlife.',
    effect: 'Disadvantage on Charisma checks for 1d6 hours.',
    price: '50 Silver'
  },
  {
    name: 'Squid Ink',
    color: 'Black',
    description: 'Goes down slowly and coats your mouth and throat. Usually followed by a chaser.',
    effect: 'Advantage on hiding or escaping during the rest of the session.',
    price: '3 Gold'
  },
  {
    name: 'Twisty',
    color: 'White yogurt on top, purplish liquid underneath',
    description:
      'Topped in a thick layer of savory yogurt, it is both a drink and meal in one. The drink itself contains the flavours of blueberries mixed in with a helping of spirits.',
    effect:
      'You feel youthful, almost as if the drink has taken you a few years back into your past. You gain +1 in Constitution and also heal 1d4 in hp per drink.',
    price: '30 Copper'
  },
  {
    name: 'Treant Tea',
    color: 'Dark brown with green swirls',
    description:
      'Served in a tall earthenware mug, this dark tea still has some ground Treant leaves floating around in it to steep. It is advised to drink slowly as not to swallow the leaves, as they leave a bitter aftertaste. The tea itself is quite sweet, but can be further sweetened to taste by putting a few twigs of sweetwood into the mug. It leaves a nutty aftertaste and an uncanny feeling of connectedness with everything made of wood in your general vicinity.',
    effect:
      'While slowly sipping the hot tea, you sprout small twigs and leaves from your orifices. As you continue drinking the twigs spread and your skin takes on a barklike appearance. For an hour after finishing the drink, your skin is solid wood, acting like the Barkskin spell, and you have disadvantage on saving throws against fire.',
    price: '25 Silver'
  },
  {
    name: "Demon's Blood",
    color: 'Black with red spots inside it',
    description: "A drink preferred by fiends and devils. Made from Cinnamon Bark and of course demon's blood.",
    effect:
      'If any other creature other than a fiend, a Tiefling, and a devil drinks this they take 1d10 Necrotic Damage. They also must make a DC 10 Con save or be poisoned taking 1d4 poison damage every hour. If poisoned roll a 1d6 to determine how many hours the creature is affected by the poison. The creature cannot heal while poisoned by the drink.If the creature falls unconscious the poison stops until the creature is awake. The creature can still be stabilized.',
    price: '5 Silver'
  },
  {
    name: 'Dragon Poison Ale',
    color: 'Dark Green with Bubbles',
    description:
      "A drink made with Poison that normally use to kill Dragons, however the amount used is very minimum that it won't be fatal to drinkers",
    effect:
      'Anyone who drinks this needs to make a DC15 con saving throw. On a fail they will be knocked out for 10mins, on a pass, they gain 10 temporary hit points for 10mins',
    price: '10 Gold'
  },
  {
    name: 'Glass Milk',
    color: 'Clearish-white',
    description:
      'An expensive drink popular among desert dwellers. However, it is not for the faint of heart (or loot!)',
    effect:
      "If you drink this and pay up, you'll be fine and get a +4 to persuasion until you succeed on a persuasion check. However, if you refuse to pay you will be cursed with some form of madness for an hour or until you pass out- this does not apply who anyone born in the desert, since they have usually drank this at least once growing up and always receive the +4 persuasion bonus.",
    price: '20 Gold'
  },
  {
    name: "Hangman's Noose",
    color: 'Yellowish brown',
    description:
      'A depressing drink that makes the consumer feel like nothing is working out, almost like a dark cloud looming over their head. Tastes a little salty on the palette, almost like tears.',
    effect:
      'The consumer feels a depression overcome him. If they are already depressed, this effect amplifies any anger, resentment, self-pity, sadness or any other negative emotions that cloud their mind. They take a 1d12 temporary penalty to Wisdom that lasts for 12 hours.',
    price: '5 Copper'
  },
  {
    name: 'Wyvern wine',
    color: 'Green with purple swirls',
    description:
      'This drink, like absinth, has mild hallucinogenic properties.  Its sweet and tangy with a slight burning aftertaste as the neurotoxin from the wyvern poison comes into contact the the soft tissue in the mouth.',
    effect:
      'fort save dc 14 or suffer from mild hallucinations, they are distracting and give the char a -2 on all skill checks and attack rolls. the effect lasts 2 hours.',
    price: '8 Gold'
  },
  {
    name: 'Dry Heave',
    color: 'White',
    description: 'Sour at first, then burns with a vengeance. After taste leaves a lingering effect.',
    effect: 'Intimidation +2 for 1d4 rounds. Fort Save DC 17 or suffer from dry heaves lasting 1d4 rounds.',
    price: '3 Copper'
  },
  {
    name: 'Giftmouth',
    color: 'Red wine with a dash of purple',
    description:
      "It's short and sweet and leaves you light on your feet. Makes you feel smart and attentive. You begin to notice things that you hadn't been able to comprehend before. A popular drink among aristocrats and nobles.",
    effect:
      "Effect: Allows the consumer to understand 1 random language for 1d4 in hours (one they previously don't know or aren't fluent in). They also gain a +5 to Perception or Notice rolls. Once the effect wears off, they will return to normal and if they hadn't learned the language previously before consuming, won't be able to understand it.",
    price: '10 Gold'
  },
  {
    name: "Bard's Blessing",
    color: 'A soft palette of subtle reds, pinks and yellows.',
    description:
      "The flavours of different fruits hit your tongue but you're unable to describe which it is before it changes to the next. As you drink, you'll feel it pooling around your voice box, stroking at it, almost as if it is tempting you to sing.",
    effect:
      'Effect: You suddenly feel like you could sing to the world! You gain a +5 to any Performance (Sing) Rolls at the cost of taking on the Dazed Condition for the duration of the performance, or as long as it takes before you need to go to the bathroom.',
    price: '20 Silver'
  },
  {
    name: 'Blind Beholder',
    color: 'Dark red with large white bubbles',
    description:
      "A drink made from material found only in the Underdark. Tastes somewhat musty, but has an incredible kick; doesn't take much to make the world start spinning.",
    effect:
      'Anyone attempting to cast a spell while still tipsy or drunk from this drink risks a 50% chance of the spell backfiring. Disadvantage on any Perception checks relying on sight.',
    price: '5 Silver'
  },
  {
    name: "Travera's TIncture",
    color:
      'Phases between a deep orange and bright yellow.  Clear but with silver dust visible when the liquid is disturbed.',
    description:
      'A drink made by a dwarven wife who was dying of a terminal illness but wished to take care of her husband even in death.',
    effect:
      'Upon drinking any vessel filled with this liquid the drinker will awake warm dry clean and hunger free the following day at noon, with no recollection of any events( because there were none) of the previous evening.  The drink chooses the nearest bed and makes the owner of the bed friendly until the following evening.',
    price: '5 Gold'
  },
  {
    name: 'Banana Milkshake',
    color: 'A slightly yellow frothing milk drink with chunks of banana scattered throughout',
    description:
      "It's thick. It's creamy and oh so bananary. Add chunks of ice to make it a refreshing drink throughout the hot summer months. Child-friendly.",
    effect: 'The consumer feels refreshened and cooled down. Allows the user to gain back 1 lower level spell usage.',
    price: '3 Copper'
  },
  {
    name: "Nyssa's Nectar",
    color: 'Clear with pearl/opalesque swirls and feint random tiny glowing pink lights',
    description:
      'A delicious alchemical / alcoholic concoction. Made with Tiefling Fire vodka and a dash of a pixie sugar dust, a teaspoon of honey, and two spells, one of which is Faerie fire. The other spell is not named but its process easily followed even by the most novice spell caster.  Its an ancient spell of warlock / fey origin.  This drink designed for the patrons in the tavern, usually female, who tire of the endless rude adventuring lot, making advance after rude advance on them.  The ladies often buy this drink for the truly offending adventurer and enjoy the effects caused on the unsuspecting offender.',
    effect:
      "This drink tastes amazing. It's almost EXACTLY what the drinker was wishing for.  Nice buzz. No hangover. DC 17 CON sv to notice effects of the drink, as you enjoy your time in the tavern.  Treat as Dazed for the 1st minute.  After that minute is over, and your head is not so fuzzy, you enjoy the rest of the night, in the form of the opposite gender.  Right down to the clothes your wearing as well. Now that the shoe is on the other foot, enjoy your fellow adventurer's attention. If you were only slightly obnoxious, maybe the girls will have pity on you and you'll enjoy a \"girl's night.\"  Effects wear off after 6 hours from the last swallow.",
    price: '2 Gold for the bartender to cast it as a ritual from that dusty drink recipe tome.'
  },
  {
    name: "Kenku's Claw",
    color: 'Black with a streak of grey',
    description:
      "Served in a curved glass in that of a claw. The drink's warmth can be felt spreading throughout the body. Then what feels like a cut across your chest.",
    effect:
      'DC 16 Cons check or shout out *insert expletive*. But gives darkvision comparable to that of a Kenku for 2d4 hours. Roll a 3d6 a 3 causes you to cough up a feather.',
    price: '2 Silver'
  },
  {
    name: 'Eye of the Beholder',
    color: 'Milky white',
    description:
      "An alcoholic drink made by nomadic groups (Goliath's come to mind) in which a hardboiled egg is added to fermented goats milk.",
    effect: 'No special effects',
    price: '3 Copper for normal livestock egg, but price can increase depending on the source of the egg'
  },
  {
    name: 'Parroty Parroty Parroty Parrot',
    color: 'A clear liquid crossed with red, green and yellow stripes.',
    description:
      "The drink tastes fluffy in your mouth, and nutty. It's smooth yet, light and bubbly enough to have you wanting more. Little do you realize however is that this fluffy feeling in your chest will soon extend to your entire body. You believe you have wings now, and oh? Is that a crest? You are now a parrot, or so you believe.",
    effect:
      "Effect: The consumer thinks they become a parrot. They don't actually turn into one, they just think they are. They lose the ability to formulate proper eloquent speech as well as the will to act like a sane individual. They act like a parrot for 3 minutes before their mind is restored.",
    price: '30 Silver'
  },
  {
    name: 'Pan-Dimensional Gargle Blaster',
    color: 'A teal blue with a foamy whith froth at the top.  Served in a glass of an odd shape.',
    description:
      'Drink... but... very carefully...  This drink was made by an interdimensional being of great importance, well at least at one point he was a leader of great importance.  Noted to be the best drink in the known Universe.',
    effect:
      "The effect of drinking one is like having your brains smashed out by a slice of lemon wrapped around a large gold brick.  The Pan-Dimensional Gargle Blaster gives you all of the pleasant effects of alcohol, while shunting most of the nasty after effects to a parallel you in another dimension.\n\nJust know that any time you drink one, there's a decent chance a parallel you in another dimension is drinking one, too  The DM can make you do a Con Save 20 during the remainder of the session at any time.  On a failed save, you begin vomiting profusely and lose your next Action.",
    price: 'Free but never served more than 2'
  },
  {
    name: 'Blerry',
    color: 'Ranging from clear lavender to deep red, depending on the berries',
    description:
      'Of halfling origin, this is a fermented juice of berries, honey and spices. There are regional and seasonal variations, each with family variations. Lightly alcoholic, Berry is usually drunk to refresh rather than to intoxicate (though in quantity, this the latter can be accomplished). Some varieties are dry, but most versions are at least slightly sweet, and almost all ideally have a light carbonation. A few variations made for sale to humans are sweeter and heavier, and are used as a kind of dessert wine, but most native drinkers turn up their nose at these.',
    effect:
      'The consumer is refreshed, energized and, usually, somewhat cheered. It intoxicates like a standard lager, and takes at least a pint to get the average halfling buzzed -- a good few pints for a typical human, and a dwarf or half-orc would need more. The varieties made for humans are stronger-- like a light wine.',
    price: '1 Silver'
  },
  {
    name: 'Wild Magic Brew',
    color: 'Golden, fizzing, and sometimes sparkling',
    description:
      'Very bubbly, tastes a little sweet, and can often cause the drinker to see random sparks around them. Sometimes the sparks are even real.',
    effect:
      'After drinking, any time you try to cast a spell in the next hour, roll 1d20. On a 1, treat it as a Wild Magic Surge, as though you were a Sorcerer with the Wild Magic Sorcerous Origin. If you are a Sorcerer, you get a Wild Magic Surge on a roll of 1 or 2.',
    price: '4 Silver'
  },
  {
    name: "Goat's Milk",
    color: 'White',
    description:
      "Fresh goat's milk. What more could you want (except an actual drink)? Somewhere along the line of your order, the bartender confused your order with that of a travelling monk, a strict teetotaller.",
    effect:
      'Your bones feel enriched from the calcium in the drink. Your mother is proud of you for making the right choice. The bartender is embarrassed for giving you the wrong choice. He gives you your drink for free and hastily finds another patron to serve.',
    price: 'Free'
  },
  {
    name: 'Zozo Juice',
    color: 'Fiery red drink with thick orange bubbles that almost make it appear molten in nature.',
    description:
      "A drink thats more like cogulated blood than an ale considering how thick it is. Bubbles crawl slowly to the surface through its sanguine blood-like depths. It is an extremely spicy drink that makes it almost unbearable to consume. Only an idiot would think its a good idea to try drink this. Is also extremely flammable to the point that water and suffocation won't be able to put it out. The only way to disperse the fire is by dispelling it. Drink and use with caution.",
    effect:
      "Effect: Wow, you're a real idiot aren't you? To drink it successfully requires a Constitution Save DC of 18. Failure to meet this immediately puts the consumer into a state of complete agony where they temporarily suffer moderate to large penalties to all attributes depending on how cruel the GM feels like being (and probably how much they tried to drink) and also take physical damage based on how badly they failed. They also gain the Weakened or Petrified condition. If they succeeded in taking the drink, they gain a +10 bonus to Constitution, Strength and Intelligence and also a temporary immunity to fire and poisons whose duration is based on how successful their roll is. (Duration capped at 48 hours.) Both unsuccessful and successful drinkers also end up with a burned tongue and won't be able to taste anything for 1d4 days.",
    price: '5 Gold'
  },
  {
    name: 'Drunken Uncle',
    color: 'Thick, dark brown stout, approximately the thickness of eggnog',
    description: 'Tasty, mouth-coating, drink that tends to remind the consumer of fond family gatherings.',
    effect:
      'Player must make a constitution check of DC 13: on a failure, the PC must reveal one of their deepest, darkest secrets, or must tell an extremely inappropriate/embarrassing story.  If the PC continues to drink the drink, increase DC by one for each pint consumed.',
    price: '3 Silver'
  },
  {
    name: 'Flavoured Water',
    color: 'Clear',
    description:
      "It's a nice, tall glass of water, known for hydrating thirsty travellers exceptionally well. A slice of lemon has been added as a nice garnish. The glass is surprisingly clean as well.",
    effect:
      'You are no longer dehydrated, and you feel like this will help whatever hangover you may have had the next day. A stranger sits down beside you and orders the same thing. You strike up a conversation over your strange, shared taste in this odd environment. After some enlightening conversation the stranger gulps down the rest of his water, bids goodnight and departs. Your realize that you not only do not know his name, but he left behind a small, glass coin beside his cup. You push through the crowd to find him,  but he seems to have vanished. The coin is a Clear-water coin of Hydration: when dropped in a glass of any liquid, it turns into clean, clear, refreshing water.The coin turns to granite, and must be given a day to recharge.',
    price: '1 Electrum'
  },
  {
    name: 'Jailhouse Wine',
    color: 'A violent orange colour mixed with streaks of deep red',
    description:
      'Made famous by prisoners who smuggled in goods into their cells in order to forget the monotony of their sentences, the drink is composited of cheap and easy to get materials in order to get you blazing drunk. Has a strong sugary and fruity flavour that borders on the sensation of fermentation.',
    effect:
      'Hits you hard and hits you fast. The consumer must roll a 1d6 save otherwise they will be effected with the strong sense of paranoia for the remainder of the evening or for up to 6 hours.',
    price: '1 Copper'
  },
  {
    name: 'Dwarven Drought',
    color: 'Nut Brown',
    description:
      'Served in a deep heavy tankard, the musty oakish odor is inviting to few but those with the affinity to damp basements. Meant to fill the void of the deep dark caverns, brewed with only the finest dwarven barley this is a strong beer for the strong willed.',
    effect:
      'Increased Intimidation and Persuasion +1 for every 2drinks. Decreased Intelligence checks by 2 for every 3 drinks.',
    price: '5 Copper'
  },
  {
    name: 'Meaty Mead',
    color: 'Mustard Yellow',
    description:
      'Sweeter than the desert this mead is thick enough to hold up the spoon that harvested the honey that made it. Good luck to anyone who drinks many of these devilish treats.',
    effect:
      'Effect: The power of mead compels you: +2 for Str &Cha saving throws for 2 hours/drink. | Monster "next day"Hangover: Speed Reduced by half; + (2drinks: -3 Con, 3Drinks: -6 Con, >3drinks: - Xd4 Con where X is amount of drinks), Short Rest = DC25. Long Rest = DC 20. If PC fails a check, effect lingers until after a second long rest then DC = 0.',
    price: '2 Copper'
  },
  {
    name: 'Heart of the Halls',
    color: 'Clear',
    description:
      'Distilled from only the most select yellow mould, this sharp-tongued spirit brings vigor and life to any looking for a good fight.',
    effect: 'Temporary +2 Str and +1 Acrobatics. +5 against intimidation checks.',
    price: '1Gold'
  },
  {
    name: 'Bluenose',
    color: 'Blue',
    description:
      'This ethereal drink glows softly and emits a light mist. Light, clear and almost flavorless the drink itself is merely a means to delight in the wonderful vapors from the mist.',
    effect:
      'Eliminates overpowering smells from being distracting. The pleasant smells put you in a good mood, Advantage on Charisma checks.',
    price: '5 Silver'
  },
  {
    name: "Lover's Kiss",
    color: 'Light pink with tiny shards of white throughout',
    description:
      'A highly addictive drink as it makes the consumer believe they are kissing their lover with each sip. Can work on those who are single too. Be careful when consuming though as it encourages drinkers to forget their surroundings and to indulge in intimate acts. Has resulted in many being caught publicly masturbating and making a fool of themselves, only to wind up in jail somewhere.',
    effect:
      'Effect: The consumer makes a Constitution or Wisdom DC 10 roll. If they fail, they partake in ludicrous acts of self love, or if in the company of someone they desire, proceed to try and indulge them in their fantasies. If they succeed, they feel the desire to do these urges but retain their control and will, leaving it up to the player to decide if they give into them.',
    price: '25 Silver'
  },
  {
    name: 'Planar Guide',
    color: 'Dark purple with small silver bubbles emerging constantly',
    description:
      'A hallucinogenic drink concocted of tentacles obtained between space and time by some Warlock of the Great Old Ones.',
    effect:
      "Upon drinking this.  Roll a d4.  Your mind is instantly teleported to one of the following planes of existence for 1 min.  \n1) The 9th Level of Hell - suffer 1d8 psychic damage as you see the most terrible things imaginable  \n2) The Void- you feel an emptiness within your being for the remainder of your character's life, roleplay appropriately  \n3) Valhalla- You witness and take part in a great battle in a heavenly place. You get a +2 to all Strength checks for the next 24 hours.   \n4) Gaia- Your consciousness returns to the collective unconscious that is the world you live on.  Gain some epiphany on your current adventure that you would only know with a 20+ Knowledge Check.",
    price: '10 Gold'
  },
  {
    name: 'Citrus Tea',
    color: 'A transparent orange whose bubbles sit on the bottom of the glass',
    description:
      'An orange slice decorates the side of the glass in which it is served.Tastes of a mixture of orange, lemon and ginger with a vague hint of spice. Travellers and nobles alike ingest this drink when they have stomach upsets as it is known to aid in both indigestion as well as constipation. Is also rather good for helping cure the common cold. Can be consumed either warm or cold.',
    effect:
      'The tea cures any minor sicknesses upon consumption as well as aids in any digestive problems the drinker might be experiencing. Also grants a +2 to Cold Resistance for up to 6 hours.',
    price: '5 Copper'
  },
  {
    name: "Crazy Al's Gnomish Knock-Out Lager",
    color: 'A transparent, greenish liquid that constantly bubbles.',
    description:
      'An infamous Gnomish brew known for requiring a waiver to be signed, absolving the tavern and brewer of any damage and/or loss of life before it can be served. It comes in a black bottle, with multiple warning signs written in Common, Gnomish, and Dwarvish. Tastes absolutely foul.',
    effect:
      "Upon consumption, the drinker must immediately make a DC 15 Constitution save. If passed, the drinker immediately gains a temporary +2 boost to Strength and Constitution scores (maximum of 20) until the next long rest, has Advantage on Constitution saves until the next long rest, and has Resistance to Cold damage until the next long rest. The drinker is also immune to the drink's effects for 24 hours.                                                                                If the drinker fails the save, but does not roll a Natural 1, the imbiber is knocked unconscious for 2d6 hours. Upon regaining consciousness, the character has Disadvantage on all ability score checks, skill rolls, and saves until two long rests have been spent. Should the drinker roll a Natural 1, he or she immediately drops to 0 Hit Points, and begins rolling Death Saves.",
    price: '15 Gold per bottle'
  },
  {
    name: "Angry Tom's Furious Ale",
    color: 'A dark amber / brown butter with a thick, creamy taupe coloured head.',
    description:
      'A traditionally brewed, thick, dark ale in which black peppercorns are added to the mash and brewed to 4.2% abv.  It is rich, peppery, and not at all infuriating.',
    effect:
      'Other than the natural inclination to enjoy the drink with a thick cut steak, standard drinking rules apply',
    price: '3 Copper'
  },
  {
    name: 'Strawberry Surprise',
    color: 'A light pink',
    description:
      "Surprise! There ain't no strawberries in this drink, Strawberry Surprise, even though rosy in color, is actually mead made with the liquid from very hot peppers. It is made by brewers and sold to people who want to play tricks on their friends, or teach someone a lesson.",
    effect:
      'Must make a DC 10 Constitution save or drop the drink and appear foolish in front of their friends. PCs who make the save by more than 5 may order another, and if they pass the test again, they gain advantage in one action resolution the next day due to their boost of confidence due to the rise in popularity from their fellows.',
    price: '5 Copper'
  },
  {
    name: 'Water',
    color: 'Translucent clear liquid',
    description: 'Just cool plain water.',
    effect: 'You feel a little better and a little more hydrated.',
    price: 'Free but sometimes a charge is applied depending on the venue.'
  },
  {
    name: 'Snake Venom',
    color: "A tempered warm brown, served with a severed snake's head",
    description:
      "Isn't actually what it says it is, but when you look at the glass as the bartender hands it over to you, you can't help but wonder if you're being pranked. In the bottom of the warm brown liquid sits an inanimate head of a dead snake. The drink itself tastes bitter with an earthy tang. A potent drink, in more ways than one...",
    effect:
      'You gain a 1d10 to resist poisons for the next 12 hours. It also makes any male consumers of the drink feel intimately invigorated. They become aroused over the course of the night and instilled with sexual stamina. Consume to impress your lover with your newfound prowess.',
    price: '1 Copper'
  },
  {
    name: 'The Pepperbox',
    color: 'Light smoke-gray',
    description:
      'This drink is made in summer from Aurochs milk mixed with 6 different kinds of peppers, which is then left to ferment until winter. It has a strong, unsavory taste and his explosively hot for the throat even if you drink it cold.',
    effect:
      'More than just making people piss drunk, it also helps to fight symptoms of cold and flu. PCs who keep a flask of this can drink it to have Advantage on its next saving throw against cold environmental effects.',
    price: '1 Gold for a bottle'
  },
  {
    name: "Goat's Piss",
    color: 'Yellow',
    description:
      '"What? Eww no of course I\'m not going to drink it...What do you mean, a coward?! Pass that here and watch me down this whole thing!"',
    effect:
      "You must roll a constitution save of DC 10 to not get sick from the drink. If you fail you're nauseous the rest of the night, and several people laugh at you (1/3 of bar patrons will not take you seriously, and you have disadvantage on any Charisma checks against them). If you pass you gain the admiration of all of the bar-goers, gaining advantages on all Charisma checks at the bar the rest of the night, as well as a free drink of your choice or a free round of ale/beer for the bar.",
    price: 'Free as a challenge'
  },
  {
    name: "The Mind's Eye",
    color: 'A purplish blue liquid that sparkles and swirls like a galaxy',
    description:
      "Staring at it is almost hypnotising. Its flavour is distinctly something magical but you can't quite put your finger on what it might possibly be. The flavour changes and dances across your tongue as your mind begins to peel open, revealing what was once lost to you. One of the few alcoholic drinks to actually make you feel and be smarter upon consumption.",
    effect:
      'Effect: The consumer gains +2 to Wisdom and Intelligence and also gains +5 to all Knowledge rolls previously known by the consumer. Does not allow them to gain knowledge on a topic they know nothing about. They also wake up to an almost soul-crushing hangover as it feels like your skull has been split in two the morning after.',
    price: '20 Silver'
  },
  {
    name: 'Pixie Piss',
    color: 'A swirling myriad of rainbows',
    description:
      'Rainbows! Rainbows everywhere! This colourful drink is as much a pleasure to indulge as it is to look at.',
    effect:
      "Effect: Makes you feel funny. The consumer finds it very easy to laugh. Great to use around people who can't tell good jokes. Also, if you drink enough of it, you'll find yourself levitating slightly. It's not enough to quell fall damage but it brings that feeling of walking on air while hovering off the ground a few inches.",
    price: '50 Silver'
  },
  {
    name: "Morg's magnificent meed",
    color: 'A thick gold',
    description:
      "A magically imbued Mead, created by the Mage's guild. (Morg being the guild master.) Often brewed for Royalty and Nobility, who embrace magic.",
    effect:
      "Choose one. a) increase Intelligence by 4 for 10 minutes, (one drink) b) recall one spell used today (one drink), c) affect people's attitudes as per the suggestion spell. (one + one drink served to each person to be affected)",
    price: '50 Gold per bottle (contains 4 drinks) Limited circulation'
  },
  {
    name: 'Elven battle spirit',
    color: 'Clear',
    description:
      "A sake Brewed by the Elves, this rice wine is believed to contain the spirits of Ancient warriors, consumed to gain those warrior's courage",
    effect: 'Con +2, -1 penalty to reflex saves, +2 moral boost to saves against fear effects, for 24 hours.',
    price: '500 Gold per Jug (15-20 servings) Often given out as military rations before a battle.'
  },
  {
    name: "Gregory's groovy grog",
    color: 'Mostly clear with a light brown hue',
    description:
      'Made with a mix of Dwarven Whisky, even Brandy, and fresh lemons. Captain Gregory created this tasty drink, to serve to his crew. This drink will keep for months, making it perfect for a long voyage.',
    effect:
      'For 24 hours, you get a +2 to resist the effects of heat, thirst, and +2 against contracting diseases. also gains a +2 to con checks and fort saves for extraneous effort.',
    price: "300 Gold per Barrel (about 100 servings) Often part of a sailor's rations."
  },
  {
    name: 'The Redheaded Harlot',
    color: 'Deep amber red and thick like blood',
    description:
      "A shot of named after the taverns best lady of the evening.  It is made from fermented berries, Tree Ent Root, and Deer's blood.  The rim of the flagon or glass is also coated in honey and dipped in a red sweet powder that is to be licked before pounding down the drink.",
    effect:
      'All who drink this get Advantage on Charisma skill checks/saves toward the opposite sex while in the tavern.',
    price: '5 Gold'
  },
  {
    name: 'Ouzo',
    color: 'Clear to shimmering white',
    description:
      'It tastes like a centaur put on a shoe made out of black licorice and then kicked you in the mouth with it.',
    effect:
      'After 3 drinks, characters will either gain 2 points of Intelligence (25%), 2 points of Wisdom (25%), or will merely believe himself to have maximum possible Intelligence and Wisdom for his species (50%). Effects last 1 hour.',
    price: '5 Silver'
  },
  {
    name: 'Red Ribbon Runs Red',
    color: 'A light red that turns darker the deeper into the glass it goes',
    description:
      'A mixture of something between rosewater and wine. The more you drink, the heavier the taste becomes.',
    effect:
      "Turns the consumer's hair and clothes different shades of red for up to 1d6 hours. They also gain +2 Charisma for the same duration and are easier in attracting attention.",
    price: '10 Copper'
  },
  {
    name: 'The Dwarven Hammer',
    color: 'Black at the bottom, but gold at the top',
    description:
      'A mixture of strong, bitter ales, dark rum, and coffee.  Most people put raw sugar on top of the foam to get a "spark" effect',
    effect:
      'Male consumers will find their beard grows an inch for each mug taken in, while female consumers will feel their hair getting thicker and longer.  Effect wears off after an hour.',
    price: '15 Copper'
  },
  {
    name: "Harpy's Scream",
    color: 'In a colour similar to skin, with what looks like very thin skin floating at the top',
    description:
      'This concoction, despite its appearance, isn\'t actually that bad. It tastes a little bit bitter at first, but once you swallow the "skin" on top it becomes sweet... and then spicy. Very spicy.',
    effect:
      'Once the drink turns "spicy", the PC must roll an 11 or higher on a STR throw in order to keep themselves from screaming. If they succeed, they will gain a +2 in performance for an hour, but if they fail they will scream for the next five minutes, and probably be kicked out of... wherever they are.',
    price: '10 Copper'
  },
  {
    name: 'Green Fey',
    color: 'Lime Green',
    description: 'Made from the wormwood root but not sweetened like liquors.',
    effect:
      'This spirit requires a DC 16 con save to avoid vomiting, and suffering a level of exhaustion for 1d12 hours the following day',
    price: '40 Gold'
  },
  {
    name: 'The Beveridge Brew',
    color: 'Dark brown',
    description:
      'Made by the finest brewers in all the land, the Beveridge Brew has the initial taste of joy and happiness, but an aftertaste that reflects the texture and taste of dirt.',
    effect:
      'Sends the drinking into a dream( unknown to the player ) were something really great happens to the character. After the effect wear off the character notices that it was just a dream, and most likely throws up from the taste of dirt.',
    price: '15 Copper'
  },
  {
    name: 'Fuzzy Night',
    color: 'Opaque Purple',
    description:
      'A thick liquid that has a sweet taste, but has a slightly bitter aftertaste. Many patrons enjoy it with a spoonful of honey mixed in to cut the aftertaste.',
    effect:
      'After drinking a single mug, the events of the evening will be forgotten the next time the character takes a long rest, unless they succeed at a DC 8 Constitution saving throw. For each additional mug after the first, increase the DC by 2.',
    price: '2 Gold'
  },
  {
    name: 'Gutbuster',
    color: 'Dark brown',
    description: 'A thick dwarven brew, very rarely shared with non-dwarves.',
    effect: 'After drinking this brew, you are resist poison effects for 1d4 hours',
    price: '5 Gold'
  },
  {
    name: 'Brimstone Brew',
    color: 'Swirl of Orange and Red, glowing',
    description:
      'This odd liquid is a constantly swirling mass of orange and red, and has a faint glow to it. The brew carries a slight odor of sulphur, and is warm on the tongue.',
    effect:
      'After drinking the brew, the character must make a DC 8 Constitution saving throw or hear an unintelligible whispering plague them. However, Warlocks and other characters in the service of infernal masters gain a slight boost in their spell powers, with all damaging spells dealing +2 bonus damage, and all saving throw difficulties to resist their spells increasing by 2. Both effects last 1d4 hours.',
    price: '3 Gold'
  },
  {
    name: 'Angel Tea',
    color: 'A healthy amber with leaves of mint stirred throughout',
    description:
      'A pleasant herbal tea constructed from various plants and resources. Mint, lavender, honey, chamomile, sugar - plus other ingredients that make your whole body feel silky and smooth. Both a refreshment and a welcome sleep remedy.',
    effect:
      "You feel your eyelids grow heavy and your body beckons you to sleep. The consumer gains Restful Sleep, which allows them to gain the benefits of a full night's rest in 4 hours. Once awakened, they feel completely refreshened.",
    price: '70 Copper'
  },
  {
    name: 'The Ghillain Special',
    color: 'A dark beer served with a skewer of bacon and pickles',
    description:
      'A tall glass broth served with a kebab of pickles and bacon staked through it. The combination of beer served with the sides makes it a flavour combination to behold, mixing sour, sweet and savory all into one.',
    effect:
      'Effect: All the different flavours allows the consumer to become inspired. They gain a +5 to any Crafting rolls as well as a +1 to Dexterity. Any person who consumes this drink with the Cooking skill gains a +15 to the next meal they craft, ultimately making it delicious and filling. Watch your waistline with this drink.',
    price: '80 Silver'
  },
  {
    name: 'Gut Shaker',
    color: 'Yellow-brown, lots of body',
    description:
      "Made from fermented goat's milk and a proprietary blend of psychoactive deep mushrooms, this dwarven brew preferred by Battleragers is not for the faint of heart or stomach.  It tastes like a mixture of hangover bile and someone punching you in the face repeatedly with a spiked gauntlet.",
    effect:
      'You become enraged, as the barbarian ability, but you are unable to voluntarily end this for 1 minute; after the effect wears off, gain one level of exhaustion',
    price: '20 Silver'
  },
  {
    name: 'Hop, Skip, & Go Naked',
    color: 'Light green',
    description:
      'A strong Mead that has a very high alcohol content.  Brewed from the best barley, hops, and herbs in the land.  Is known to get the local townsfolk to do things they normally might not do with just one goblet.  Hence the name.',
    effect:
      'Any character that drinks from this must Roll on ANY Carousing Table.  I suggest the https://www.reddit.com/r/DnD/comments/3uunbt/the_bigger_badder_longer_uncut_d100_carousing/',
    price: '1 Gold'
  },
  {
    name: 'Sea Shanty',
    color: 'Aqua blue, almost azure',
    description:
      'The drink swirls constantly in the glass, tugged downward like a whirlpool. This constant animation makes it a little annoying to consume as it is prone to spill.',
    effect:
      'The consumer gets drunk, but is able to breathe underwater for approximately 1 minute. Swim drunkenly at your own risk.',
    price: '2 Gold'
  },
  {
    name: 'Bloody Medusa',
    color: "Thick and red as blood but with green-ish swirls.  Served with a green snake's head on the rim.",
    description:
      'Some think that the drink originally started out using crushed up vegetables and fermented alcohol.  Some recipes still do use the original recipe, but have decided to actually add in the blood of a Medusa.  Not only does it taste great for a morning pick me up, but it also grants the drinkers who partake with a special surprise.  Be wary not to go picking a fight in a bar where this is served...',
    effect:
      'Drinking this concoction is as if casting Stoneskin on oneself.  Creature has resistance to non-magical bludgeoning, piercing, and slashing damage for 1 hour.',
    price: '10 Gold'
  },
  {
    name: "Merchant's Lucky Day",
    color: 'A simmering brown with tiny golden flecks swirling throughout its depths',
    description:
      "The drink tastes almost like copper, like you'd been licking coins or pipes all day long. The metallic aftertaste however slowly begins to whittle down the more you drink of it, and eventually becomes something enjoyable.",
    effect:
      'Effect: You feel lucky, so lucky in fact that any ability or action taken to gain money means you get a slightly increased income. You also have a higher chance of finding loot and items.',
    price: '2 Gold'
  },
  {
    name: 'Butterbeer',
    color: 'A buttery brown drink with a creamy foaming top',
    description:
      'Foaming, smooth to taste with enough kick to keep itself interesting. A popular purchase among novice drinkers.',
    effect:
      'The consumer feels a little more alert and sociable. They gain a +1 to Charisma and Perception checks. This effect only lasts an hour.',
    price: '20 Copper'
  },
  {
    name: 'Cthulhubrew',
    color: 'A slimy green color and thick like syrup.',
    description:
      'No one knows where this concoction comes from or what exactly it is made from.  Strange monks in hooded robes deliver the drink by nightfall and give it at a highly discounted rate.  Some say that drinking enough of it not only does the desired effects of alcohol but also enlightens their brain chemistry to a high state.  The taste of it is like Licorice or Absynth',
    effect:
      'Once consumed, the player gets Telekinesis for the next 24 hours.  But in exchange the player hears dissonant whispers from another being calling them to join their cult.  Deals 1d6 Psychic Damage.',
    price: '90 Copper'
  },
  {
    name: 'Fire Ball',
    color: 'Fiery Red',
    description:
      'Have you ever wondered what hell fire would taste like? Then fireball is for you! "Man i chugged a quart of fireball before the dance and the guards were all over my ass  as soon as i walked in." Best night ever!',
    effect:
      'After drinking a fireball shot you feel the hot embrace of hell in your lungs. You get resistance to cold damage for the next 30 minutes.',
    price: '9 Silver per Shot'
  },
  {
    name: 'Hellfire Whisky',
    color: 'Deep Dark Red',
    description:
      'Brewed by Demons, Devils, and even Dwarves. It is said that Hades himself came up with this drink as a way to get himself drunk. Primarily Tastes of Strong Whisky and Lots of Cinnamon.',
    effect:
      'Drinking this could cause death, partial intelligence loss, or just a slight buzz. The GM should select what is right for their campaign and situation. Before doing so, ask yourself "What would add the most to the story currently?"',
    price: '5 Gold per Shot'
  },
  {
    name: 'Tamaranian Tanqueray & Zorkaberry Tonic',
    color: 'Cherry red with Purple swirls and glowing green fruit',
    description: 'An extremely rare drink, obtained by a group of Spelljamming adventurers. Fruity taste.',
    effect:
      'DC 13 CON sv.  Success = The ability to levitate for the next 3 hours, and + 2 STR.  Failure = A splitting headache and Disadvantage on WIS checks for the next 3 hours and complain like a bloated Clorbag.',
    price: "2 Gold (Unless it's Blorthog's Day, then your Friend is buying)"
  },
  {
    name: 'Lizard Spit',
    color: 'Bubbly Clear',
    description:
      "Gathered from the three Dire Mangrove Monitors behind T'hal's Tavern, this drink smells of rotting flesh and metal. T'hal will give anyone who can keep this drink down 2gp and a place on his tavern wall with their name on it.",
    effect:
      "It smells of rotting goblin flesh T'hal feeds his beasts. Drinking it requires as Con save with a DC of 18, or risk vomiting and taking 2d6 necrotic damage.",
    price: '4 Silver a Pint'
  },
  {
    name: "Dragon's Milk",
    color: 'A deep rusty brown, almost black with toffee colored bubbles, almost opaque when held to the light',
    description:
      'Brewed in a secretive enclave known who mark their bottles with the rune of Othala. Born of a mixing of an ancient dwarven recipe with skill and flavor from members of all the civilized races and it is even said the aid of a few dragons. This brew has a strong flavor of chocolatey grains, vanilla, bitter hops, and a myriad of spices appealing differently to each imbiber. It is most commonly found in dwarven enclaves and elven kingdoms but has been making headway into the taverns of all the civilized races; though with limited supply this is leading to an overall price increase over time making it ever more popular among nobles as a status symbol.',
    effect:
      'This dank brew instantly warms the drinker and produces a profound euphoria. This brew removes one level of exhaustion as well as making the drinker immune to the ill effects of cold weather, the frightened condition, the poisoned condition and damage for an hour after drinking. It also tends to make its drinkers foolhardy as the euphoria and lack of fear associated with drinking it leads some to disregard their own safety, this has lead to bartenders being careful to whom they serve this amazing brew.',
    price: '25 Gold per pint, 310 PP per keg (taverns tend to buy at roughly 250 PP per keg)'
  },
  {
    name: 'One Beer to Rule Them All',
    color: 'A clear liquid but when held to heat, elven writing appears as fiery wisps throughout the drink',
    description:
      "The drink was brewed by an Evil Dark lord of a land far far away from this tavern.  Some say that they don't even know how long ago it was brewed.  It is extremely Rare and the contents of a barrel of it has been known to be killed over.  It is known to taste like the absolute best drink one has ever had in their lifetime.  Due to creatures having different tastes, it mimics to their desired favorite flavor magically.",
    effect:
      'On consumption, any character must back a Wisdom Save of DC 20.  On a failed save, their alignment changes to Chaotic Evil for the next 24 hours.  After the 24 hours they must roll a Constitution Save of DC 16 or think of nothing but trying to get more of this drink. If they fail this and do not have the drink in the next 24 hours after, they no longer have to make saving throws (they have been detoxed long enough)',
    price: "1 Platinum per Pint or Free by doing the tavern owner a favor (GM's discretion)"
  },
  {
    name: 'Glavgalask',
    color: 'A golden brown',
    description:
      "The translation of this drink's name is an old, orcish curse - one you don't say in polite company. Known for being one of the toughest drinks ever, few people can handle this and not end up on their backside. It is rarely drunk purely for fun, generally it is drunk in ceremonies and competitions.",
    effect:
      'The drinker must make a DC 15 constitution saving throw. On a success the drinker is poisoned for one minute. On a failure the drinker falls unconscious for 1d4 hours. If this drink is mixed into food all diseases are removed from it. Any food that has this drink mixed in does not force the constitution save that drinking it straight would cause.',
    price: '100 Gold for a pint. You drink it in shot glasses.'
  },
  {
    name: 'Shirley Temple of Elemental Evil',
    color: 'Swirly chaotic mix',
    description: 'Incorporates the various base elements together, all with an evil taint.',
    effect:
      'Gives visions of evil elemental creatures nearby for d20 minutes/drink.  On a roll of 1 on a d6, one of the following occurs (roll 1d4):  1 - Drinker is turned into a Water Weird for 1d8 hours, but retains his or her mind.  2 - Drinker grows 1d20 mushrooms on their body, giving -4 to Charisma until cured with Cure Disease; broken mushrooms grow back within one hour.  3 - Drinker catches fire for 1d4 rounds, doing 1d4 damage per round - flammable mundane items such as clothes or axe handles are destroyed.  4 - Drinker finds it difficult to breathe for 1d4 hours, causing dizziness and -2 Constitution for the duration.',
    price: '1 Silver per Glass'
  },
  {
    name: 'Glitter',
    color: 'Clear with flecks of gold and silver',
    description:
      "A sweet, light craft beer brewed from magically grown sugar. It's is often drunk at parties and given as a gift between nobles.\nMust be bought from the distiller. Will only be sold to those of upper class or renown.",
    effect:
      'Roll a d20. On a 19-20, the drinker is cured of all non-magical poisons and diseases. A bottle contains 40 drinks.',
    price: 'Generally sold for 100 Gold for a 2 pint bottle.'
  },
  {
    name: 'Skullbungle',
    color: 'Jet black with red highlights',
    description:
      'A drink made by distilling strong dwarves spirits through a mash made of hallucinogenic mushrooms and powerful chillies. Staggeringly strong and harsh yet incredibly tasty. Imbibing the drink often produces soothing hallucinations, making this a popular drink among the downtrodden as well. Bottles are generally bought right from the distillery. A popular gift among nobles with a penchant for booze such as Dwarves, Orcs and Stoutblood halflings.',
    effect:
      'The drinker must make a DC 15 constitution saving throw or be poisoned for 1 minute. While poisoned in this manner the character cannot be frightened or charmed.',
    price: '5 Silver per shot.'
  },
  {
    name: 'Scumble',
    color: 'Muddy Brown',
    description: "It's made from apples, mostly. Slightly fizzy,don't let it contact metal.",
    effect:
      '1 shot = Acts as a potion of Heroism. 3 shots = memory loss of the next 12 hours. CON save (-2 per shot) or incapacitating hangover for the next day.',
    price: '1 Silver per Shot'
  },
  {
    name: 'Brewmosa',
    color: 'Gold',
    description: '2 freshly squeezed oranges from a local orchard mixed with a pint of mead.',
    effect: 'Second Wind - Restore Health: Roll 4d4 divide by 4',
    price: '4 Copper'
  },
  {
    name: 'The Mindflayer',
    color: 'Blueish Purple',
    description:
      "Brewed with the sole intent of forgetting whatever trouble you may cause under it's influence. The Mindflayer, has had many of its imbibers waking up in jail, or naked in a puddle of their own vomit and piss, or even one poor fella in bed with a deformed goblin named Bob...",
    effect:
      'Only 1 shot is needed for "I DID WHAT?!" to take effect...  "I DID WHAT?!": Until your next long rest your INT is lowered by 10, afterwards until your second long rest your INT is lowered by 5. Note: Your Int can never decrease below 1.',
    price: '1 Gold per Shot'
  }
];
