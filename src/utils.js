const names = require('./data/names');

const getRandomItemInArray = arr => arr[Math.floor(Math.random() * arr.length)];

module.exports = {
  randomInteger: (max, min = 1) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  },

  parseToNumber: string => {
    const number = parseInt(string.split(' ')[0]);
    return isNaN(number) ? null : number;
  },

  getItemFromArray: (body, array) => {
    const index = body.text && utils.parseToNumber(body.text);
    return index && index <= array.length
      ? array[index - 1]
      : array[Math.floor(Math.random() * (array.length - 1)) + 1];
  },

  getRandomName: (count = 1) => {
    const { firstName, lastNameOne, lastNameTwo } = names;
    const namesArr = [];
    for (let i = 0; i < count; i++) {
      namesArr.push(
        `*${getRandomItemInArray(firstName)} ${getRandomItemInArray(lastNameOne)}${getRandomItemInArray(lastNameTwo)}*`
      );
    }
    return namesArr.join(', ');
  },

  joinArrayOfText: text => {
    return text.constructor === Array ? text.join(' ') : text;
  },

  capitalizeFirstLetter: string => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
};
