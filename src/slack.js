const utils = require("./utils");

module.exports = {
  formatLeaderboard: leaderboard => {
    const fields = leaderboard.map((l, i) => ({
      title: `${i + 1}: ${l.master.name}`
    }));
    return {
      attachments: [
        {
          fallback: `The snackiest master right now is non other than ${
            leaderboard[0].master.name
          } :tada:`,
          color: "#f6bd60",
          fields
        }
      ]
    };
  },
  formatDrink: drink => {
    return {
      attachments: [
        {
          fallback: drink.name,
          color: "#06D6A0",
          fields: [
            { title: "Name", value: drink.name, short: true },
            { title: "Price", value: drink.price, short: true },
            { title: "Color", value: drink.color, short: false },
            { title: "Description", value: drink.description, short: false },
            { title: "Effect", value: drink.effect, short: false }
          ]
        }
      ]
    };
  },

  formatPoison: poison => {
    return {
      attachments: [
        {
          fallback: poison.name,
          color: "#724CF9",
          fields: [
            { title: "Name", value: poison.name, short: true },
            { title: "Type", value: poison.type, short: true },
            { title: "Price", value: poison.price, short: false },
            { title: "Description", value: poison.description, short: false },
            { title: "Effect", value: poison.effect, short: false }
          ]
        }
      ]
    };
  },

  formatMonster: monster => {
    const {
      name,
      size,
      type,
      subtype,
      alignment,
      ac,
      hp,
      speed,
      str,
      dex,
      con,
      int,
      wis,
      cha,
      skill,
      resist,
      vulnerable,
      immune,
      conditionImmune,
      senses,
      passive,
      languages,
      cr,
      trait,
      action,
      legendary,
      spells,
      slots
    } = monster;

    const traits =
      trait &&
      trait.map(t => ({ title: t.name, value: utils.joinArrayOfText(t.text) }));
    const actions =
      action &&
      action.map(a => ({
        title: a.name,
        value: a.text
      }));
    const legendaries =
      legendary &&
      legendary.map(l => ({
        title: l.name,
        value: utils.joinArrayOfText(l.text)
      }));

    return {
      attachments: [
        {
          fallback: name,
          color: "#f78ca0",
          title: name,
          fields: [
            type && {
              title: "Type",
              value: `${utils.capitalizeFirstLetter(type)} ${
                subtype ? `_(${utils.capitalizeFirstLetter(subtype)})_` : ""
              }`,
              short: true
            },
            alignment && {
              title: "Alignment",
              value: utils.capitalizeFirstLetter(alignment),
              short: true
            },
            cr && { title: "Challenge", value: cr, short: true },
            ac && { title: "Armor Class", value: ac, short: true },
            hp && { title: "Hit Points", value: hp, short: true },
            speed && { title: "Speed", value: speed, short: true },
            size && { title: "Size", value: size, short: true },
            {
              title: "Attributes",
              value: `STR: ${str}\tDEX: ${dex}\tCON: ${con}\tINT: ${int}\tWIS: ${wis}\tCHA: ${cha}`
            },
            senses && {
              title: "Senses",
              value: utils.capitalizeFirstLetter(senses),
              short: true
            },
            passive && {
              title: "Passive perception",
              value: passive,
              short: true
            },
            languages && {
              title: "Languages",
              value: utils.capitalizeFirstLetter(languages),
              short: true
            },
            skill && {
              title: "Skills",
              value: utils.capitalizeFirstLetter(skill),
              short: true
            },
            resist && {
              title: "Resist",
              value: utils.capitalizeFirstLetter(resist),
              short: true
            },
            immune && {
              title: "Immune",
              value: utils.capitalizeFirstLetter(immune),
              short: true
            },
            conditionImmune && {
              title: "Condition immunity",
              value: utils.capitalizeFirstLetter(conditionImmune),
              short: true
            },
            vulnerable && {
              title: "Vulnerabilities",
              value: utils.capitalizeFirstLetter(vulnerable),
              short: true
            },
            ...traits,
            ...actions,
            ...legendaries,
            spells && { title: "Known spells", value: spells },
            slots && { title: "Spell slots", value: slots }
          ]
        }
      ]
    };
  }
};
