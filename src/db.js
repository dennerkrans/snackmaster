const db = require('mongoose');

db.Promise = global.Promise;

db.connect(`mongodb://${process.env.DB_USER}:${process.env.DB_PASSWORD}@ds223268.mlab.com:23268/dungeons-and-dbs`);

module.exports = db;
