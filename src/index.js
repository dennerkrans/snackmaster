//require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");

const db = require("./db");
const routes = require("./routes");

const port = process.env.PORT || 3000;

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use("/", routes);

app.listen(port, () => console.log(`Listening on ${port}`));
