const routes = require("express").Router();
const axios = require("axios");

const utils = require("./utils");
const slack = require("./slack");
const data = require("./data");
const models = require("./models");

routes.get("/", (req, res) => {
  res.send("Up and 🏃‍💨");
});

routes.post("/snackmaster", async (req, res) => {
  try {
    const { data } = await axios.get(
      "https://snackmaster.journeyofthelemaitre.wang/snackmaster/current"
    );
    res.json({
      response_type: "in_channel",
      text: `The current s n a c k m a s t e r is _${
        data.master.name
      }_ and will bring those delicious _${
        data.cuisine.snacks
      }_ snacks :watermelon:. And what _${data.cuisine.adjective} ${
        data.cuisine.type
      }_ food will we chow down on :yum:?`
    });
  } catch (e) {
    res.json({
      response_type: "ephemeral",
      text:
        "Oops! Something went wrong :scream: Better check if https://snackmaster.journeyofthelemaitre.wang is still operational"
    });
  }
});

routes.post("/snackiestmaster", async (req, res) => {
  try {
    const { data } = await axios.get(
      "https://snackmaster.journeyofthelemaitre.wang/snackmaster/stats"
    );
    const leaderboard = data.masters;
    leaderboard.sort((a, b) => b.incidence.absolute - a.incidence.absolute);
    res.json(slack.formatLeaderboard(leaderboard));
  } catch (e) {
    res.json({
      response_type: "ephemeral",
      text: "Oops! No stats! Please come again!"
    });
  }
});

routes.post("/name", (req, res) => {
  const { body } = req;
  const number = body.text && utils.parseToNumber(body.text);
  const names = number ? utils.getRandomName(number) : utils.getRandomName();
  res.json({
    text: names
  });
});

routes.post("/trinket", (req, res) => {
  const { body } = req;
  const number = body.text && utils.parseToNumber(body.text);
  const trinket = utils.getItemFromArray(number, data.trinkets);
  res.json({
    text: trinket
  });
});

routes.post("/drink", ({ body }, res) => {
  const drink = utils.getItemFromArray(body, data.drinks);
  res.json(slack.formatDrink(drink));
});

routes.post("/poison", ({ body }, res) => {
  const poison = utils.getItemFromArray(body, data.poisons);
  res.json(slack.formatPoison(poison));
});

routes.post("/monster", ({ body }, res) => {
  models.Monster.findOne()
    .skip(utils.randomInteger(process.env.MONSTER_COUNT))
    .then((monster, err) => {
      res.json(slack.formatMonster(monster));
    });
});

module.exports = routes;
