const mongoose = require('mongoose');

const MonsterSchema = mongoose.Schema({
  name: String,
  size: String,
  type: String,
  subtype: String,
  alignment: String,
  ac: String,
  hp: String,
  speed: String,
  str: String,
  dex: String,
  con: String,
  int: String,
  wis: String,
  cha: String,
  skill: String,
  resist: String,
  vulnerable: String,
  immune: String,
  conditionImmune: String,
  senses: String,
  passive: String,
  languages: String,
  cr: String,
  trait: [
    {
      name: String,
      text: String | [String]
    }
  ],
  action: [
    {
      name: String,
      text: String,
      attack: String | [String]
    }
  ],
  legendary: [
    {
      name: String,
      text: String | [String]
    }
  ],
  spells: String,
  slots: String
});

const Monster = mongoose.model('monsters', MonsterSchema);

module.exports = {
  Monster
};
